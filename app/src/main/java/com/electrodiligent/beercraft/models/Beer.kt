package com.electrodiligent.beercraft.models

data class Beer(
    val id: Double,
    val name: String,
    val style: String,
    val abv: String,
    val ounces: Double) {


}