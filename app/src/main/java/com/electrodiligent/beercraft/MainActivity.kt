package com.electrodiligent.beercraft

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.TextureView
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.electrodiligent.beercraft.adapteres.BeerAdapter
import com.electrodiligent.beercraft.models.Beer
import com.electrodiligent.beercraft.services.BeerApi

import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var rv: RecyclerView
     var beerList : List<Beer> = listOf()
    lateinit var beerAdapter: BeerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        beerAdapter = BeerAdapter()
        beerAdapter.data = beerList
        rv = findViewById(R.id.recyclerView)

        rv.adapter = beerAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onStart() {
        super.onStart()
        val beerAdapter = BeerAdapter()
        rv.adapter = beerAdapter

        BeerApi.retrofitService.getProperties().enqueue(object : Callback<List<Beer>> {
            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                val v = "Failure: " + t.message
                val tv = findViewById<TextView>(R.id.textView)
                tv.visibility = View.VISIBLE
                tv.text = v
            }

            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                val v = response.body()
                findViewById<TextView>(R.id.textView).text = "Total beers : " + v?.size.toString()
                beerList = v!!
                beerAdapter.data = beerList
                beerAdapter.notifyDataSetChanged()
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
