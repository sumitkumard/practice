package com.electrodiligent.beercraft.services

import com.electrodiligent.beercraft.models.Beer
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


private const val BASE_URL = "http://starlord.hackerearth.com"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()



interface  BeerApiService {
    @GET("beercraft")
    fun getProperties():
            Call<List<Beer>>
}


object BeerApi {
    val retrofitService : BeerApiService by lazy {
        retrofit.create(BeerApiService::class.java)
    }
}