package com.electrodiligent.beercraft.adapteres.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.electrodiligent.beercraft.R
import com.electrodiligent.beercraft.models.Beer

class BeerViewHolder (val view: View): RecyclerView.ViewHolder(view) {

    fun bindItems(beer: Beer) {
        val textViewName = itemView.findViewById(R.id.nameTextView) as TextView
        val textViewStyle = itemView.findViewById(R.id.beerStyleTextView) as TextView
        val textViewABV = itemView.findViewById(R.id.abvTextView) as TextView
        textViewName.text = beer.name
        textViewStyle.text = beer.style
        textViewABV.text = beer.abv


    }
}