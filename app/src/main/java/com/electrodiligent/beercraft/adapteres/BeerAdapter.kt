package com.electrodiligent.beercraft.adapteres

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.electrodiligent.beercraft.R
import com.electrodiligent.beercraft.adapteres.viewholders.BeerViewHolder
import com.electrodiligent.beercraft.models.Beer


class BeerAdapter : RecyclerView.Adapter<BeerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cell_item,parent,false)
        return BeerViewHolder(view)
    }

    var data = listOf<Beer>()

    override fun getItemCount(): Int {

        return data.count()
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        val item = data[position]
        holder.bindItems(item)

      }
}